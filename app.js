var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require("body-parser");
const cors = require('cors');
var LambdaRoute = require('./routes/LambdaRoute');
var ApiGatewayRoute = require('./routes/ApiGatewayRoute');
var AuroraServerlessRoute = require('./routes/AuroraServerlessRoute');
var DynamoDbRoute = require('./routes/DynamoDbRoute');
var S3Route = require('./routes/S3Route');
var SnsRoute = require('./routes/SnsRoute');
var SqsRoute = require('./routes/SqsRoute');
var AzureFunctionsRoute = require('./routes/AzureFunctionsRoute');
var ApiManagementRoute = require('./routes/ApiManagementRoute');
var AzureNotificationHubsRoute = require('./routes/AzureNotificationHubsRoute');
var AzureServiceBusRoute = require('./routes/AzureServiceBusRoute');
var BlobStorageRoute = require('./routes/BlobStorageRoute');
var CosmoDbRoute = require('./routes/CosmoDbRoute');
var QueueStorageRoute = require('./routes/QueueStorageRoute');
var SqlDataBaseRoute = require('./routes/SqlDataBaseRoute');


var app = express();
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());


app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
      "Access-Control-Allow-Methods",
      "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  res.setHeader('Access-Control-Allow-Credentials', true); //

  next();
});
app.use('/Lambda', LambdaRoute);
app.use('/ApiGateway', ApiGatewayRoute);
app.use('/AuroraServerless', AuroraServerlessRoute);
app.use('/DynamoDB', DynamoDbRoute);
app.use('/S3', S3Route);
app.use('/SNS', SnsRoute);
app.use('/SQS', SqsRoute);
app.use('/AzureFunctions', AzureFunctionsRoute);
app.use('/ApiManagement', ApiManagementRoute);
app.use('/AzureNotificationHubs', AzureNotificationHubsRoute);
app.use('/AzureServiceBus', AzureServiceBusRoute);
app.use('/BlobStorage', BlobStorageRoute);
app.use('/CosmoDb', CosmoDbRoute);
app.use('/QueueStorage', QueueStorageRoute);
app.use('/SqlDataBase', SqlDataBaseRoute);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.status(401).json({
    message: "erreur"
  });
});

const port = process.env.PORT || 3000;
app.listen(port, function () {
  console.log('Pisquare Calculation app listening on port 3000!')
});

module.exports = app;
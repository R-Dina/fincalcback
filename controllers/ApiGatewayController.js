const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to to calculate the Http Apis of Api Gateway service consumption

exports.getApiGatewayHttp = (req, res, next) => {
    var sizeReq=parseInt(req.body.sizeReq);
    var numReq;
    var max;
    var numRequests=parseInt(req.body.numRequests);
    var totalNumReq;
    var cost;
    var AverageSizeReq=sizeReq*1024;
    numReq=AverageSizeReq / 512;
    max=Math.round(numReq);
    totalNumReq=numRequests*max;

    if(totalNumReq>=300000000)
    {

        var cost1=300000000*0.0000010000;
        var cost2=(totalNumReq-300000000)*0.0000009000;
         cost=cost1+cost2;
    }
    else {
         cost=totalNumReq*0.0000010000;
    }

    return res.status(200).json({
        cost
    });

};

//The following method allows you to to calculate the Rest Apis  consumption

exports.getApiGatewayRest = (req, res, next) => {
    var numReq=(parseInt(req.body.numReq))*1000000;
    var memory=parseFloat(req.body.memory);
    var memoryPrice;
    var Restotal;
    var cost;
    var cost1;
    var cost2;
    var cost3;
    var cost4;

    if(numReq<=333000000)
    {
        cost=numReq*0.0000035000;
        console.log("cost Rest "+cost);
    }
    else if(numReq>333000000 && numReq<=1000000000 ) {
        cost1 = 333000000 * 0.0000035000;
        cost2 = (numReq - 333000000) * 0.0000028000;
        cost=cost1+cost2;

    }
        if(numReq>1000000000 && numReq<=20000000000)
        { cost1 = 333000000 * 0.0000035000;
          cost2 = 667000000* 0.0000028000;
          cost3=  (numReq -1000000000)*0.0000023800 ;
          cost=cost1+cost2+cost3;
        }
        if(numReq>20000000000) {
          cost1 = 333000000 * 0.0000035000;
          cost2 = 667000000* 0.0000028000;
          cost3=  19000000000  *0.0000023800;
          cost4=  (numReq -20000000000)*0.0000015100  ;

          cost=cost1+cost2+cost3+cost4;
        }

    if (memory==0)
    {
        memoryPrice=0;
    }
    else if (memory==0.5)
    {
        memoryPrice=0.02*730;
    }
    else if (memory==1.6)
    {
        memoryPrice=0.038*730;
    }
    else if (memory==6.1)
    {
        memoryPrice=0.2*730;
    }
    else if (memory==13.5)
    {
        memoryPrice=0.25*730;
    }
    else if (memory==28.4)
    {
        memoryPrice=0.50*730;
    }
    else if (memory==58.2)
    {
        memoryPrice=1.00*730;
    }
    else if (memory==118)
    {
        memoryPrice=1.90*730;
    }
    else if (memory==237)
    {
        memoryPrice=3.80*730;
    }

     Restotal=memoryPrice+cost;

    return res.status(200).json({
       Restotal
    });
};
//The following method allows you to to calculate the WebSocket Apis  consumption

exports.getApiGatewayWebSocket = (req, res, next) => {
   var messages=parseInt(req.body.messages);
   var averageConnectionDuration=parseFloat(req.body.averageConnectionDuration);
   var averageConnectionRate=parseInt(req.body.averageConnectionRate);
   var averageMessageSize=parseInt(req.body.averageMessageSize);
  var FirstMilliardMessagesPrice;
  var TheRestPrice;
  var TotalPriceOfWebSmessages;
   var messagesPerMonth=messages*(60*60*730);
   var averageConnectionRatePerMonth=averageConnectionRate*(60*60*730);
   var averageConnectionDurationPerMonth=averageConnectionDuration/60;
   var billableMessages=averageMessageSize/32;
   var RoundBillableMessages=Math.ceil(billableMessages);
   console.log("round "+RoundBillableMessages);

   var BillableWebSocketMessages=messagesPerMonth*RoundBillableMessages;
   if(BillableWebSocketMessages>=1000000000)
   {
      FirstMilliardMessagesPrice=1000000000*0.0000010000;
      TheRestPrice=(BillableWebSocketMessages-1000000000)*0.0000008000;
      TotalPriceOfWebSmessages=FirstMilliardMessagesPrice+TheRestPrice;
   }
   else {
      TotalPriceOfWebSmessages=BillableWebSocketMessages*0.0000010000;
   }

   var totalWSConnectionPrice=averageConnectionDurationPerMonth*averageConnectionRatePerMonth*0.00000025;

   var totalWebSocketApis=TotalPriceOfWebSmessages+totalWSConnectionPrice;

    return res.status(200).json({
        totalWebSocketApis
    });


};


//The following method allows you to to calculate the total  Api Gateway  consumption

exports.getApiGatewayTotal = (req, res, next) => {

    /////////////////////////////Http/////////////////////////////////////////////
  var sizeReq=parseInt(req.body.sizeReq);
  var numReq;
  var max;
  var numRequests=parseInt(req.body.numRequests);
  var totalNumReq;
  var cost;
  var AverageSizeReq=sizeReq*1024;
  numReq=AverageSizeReq / 512;
  max=Math.round(numReq);
  totalNumReq=numRequests*max;

  if(totalNumReq>=300000000)
  {

    var cost1=300000000*0.0000010000;
    var cost2=(totalNumReq-300000000)*0.0000009000;
    cost=cost1+cost2;
  }
  else {
    cost=totalNumReq*0.0000010000;
  }
    ////////////////////////////////////////REST//////////////////////////////////////
  var numReq1=parseInt(req.body.numReq1)*1000000;
  var memory=parseFloat(req.body.memory);
  var memoryPrice;
  var Restotal;
  var costt;
  var costt1;
  var costt2;
  var cost3;
  var cost4;

  if(numReq1<=333000000)
  {
    costt=numReq1*0.0000035000;
  }
  else if(numReq>333000000 && numReq<=1000000000 ) {
    costt1 = 333000000 * 0.0000035000;
    costt2 = (numReq1 - 333000000) * 0.0000028000;
    costt=costt1+costt2;

  }
  if(numReq1>1000000000 && numReq1<=20000000000)
  { costt1 = 333000000 * 0.0000035000;
    costt2 = 667000000* 0.0000028000;
    cost3=  (numReq1 -1000000000)*0.0000023800 ;
    costt=costt1+costt2+cost3;
  }
  if(numReq1>20000000000) {
    costt1 = 333000000 * 0.0000035000;
    costt2 = 667000000* 0.0000028000;
    cost3=  19000000000  *0.0000023800;
    cost4=  (numReq1 -20000000000)*0.0000015100  ;

    costt=costt1+costt2+cost3+cost4;
  }

  if (memory==0)
  {
    memoryPrice=0;
  }
  else if (memory==0.5)
  {
    memoryPrice=0.02*730;
  }
  else if (memory==1.6)
  {
    memoryPrice=0.038*730;
  }
  else if (memory==6.1)
  {
    memoryPrice=0.2*730;
  }
  else if (memory==13.5)
  {
    memoryPrice=0.25*730;
  }
  else if (memory==28.4)
  {
    memoryPrice=0.50*730;
  }
  else if (memory==58.2)
  {
    memoryPrice=1.00*730;
  }
  else if (memory==118)
  {
    memoryPrice=1.90*730;
  }
  else if (memory==237)
  {
    memoryPrice=3.80*730;
  }

  Restotal=memoryPrice+costt;

  //////////////////////////////////////////Web Socket//////////////////////////

  var messages=parseInt(req.body.messages);
  var averageConnectionDuration=parseFloat(req.body.averageConnectionDuration);
  var averageConnectionRate=parseInt(req.body.averageConnectionRate);
  var averageMessageSize=parseInt(req.body.averageMessageSize);
  var FirstMilliardMessagesPrice;
  var TheRestPrice;
  var TotalPriceOfWebSmessages;
  var messagesPerMonth=messages*(60*60*730);
  var averageConnectionRatePerMonth=averageConnectionRate*(60*60*730);
  var averageConnectionDurationPerMonth=averageConnectionDuration/60;
  var billableMessages=averageMessageSize/32;
  var RoundBillableMessages=Math.round(billableMessages);

  var BillableWebSocketMessages=messagesPerMonth*RoundBillableMessages;
  if(BillableWebSocketMessages>=1000000000)
  {
    FirstMilliardMessagesPrice=1000000000*0.0000010000;
    TheRestPrice=(BillableWebSocketMessages-1000000000)*0.0000008000;
    TotalPriceOfWebSmessages=FirstMilliardMessagesPrice+TheRestPrice;
  }
  else {
    TotalPriceOfWebSmessages=BillableWebSocketMessages*0.0000010000;
  }

  var totalWSConnectionPrice=averageConnectionDurationPerMonth*averageConnectionRatePerMonth*0.00000025;

  var totalWebSocketApis=TotalPriceOfWebSmessages+totalWSConnectionPrice;

    var ApiGatewayTotal=cost+Restotal+totalWebSocketApis;
    console.log("cost "+cost);
    console.log("Restotal "+Restotal);
    console.log("totalWebSocketApis "+totalWebSocketApis);
    return res.status(200).json({
        ApiGatewayTotal
    });
};

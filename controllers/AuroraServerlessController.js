const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to to calculate the Aurora postgreSQL settings consumption

exports.getAuroraPosgreSQLSettings = (req, res, next) => {

    var ACU=parseInt(req.body.ACU);
    var totalPostgreSQL=ACU*730*0.06;

    return res.status(200).json({
       totalPostgreSQL
    });


};


//The following method allows you to to calculate the Aurora postgreSQL data storage consumption

exports.getAuroraPosgreSQLdataStorage = (req, res, next) => {

    var storageAmount=parseFloat(req.body.storageAmount);
    var storageCost=storageAmount*0.10;
    var reads=parseInt(req.body.reads);
    var writes=parseInt(req.body.writes);
    var somme =reads+writes;
    var sommePerMonth=somme*730*60*60;
    var sommeCost=sommePerMonth*0.0000002;
   var totalPostgreSQLdataStorage=sommeCost+storageCost;

    return res.status(200).json({
        totalPostgreSQLdataStorage
    });


};


//The following method allows you to to calculate the Aurora postgreSQL backup storage consumption

exports.getAuroraPosgreSQLbackupStorage = (req, res, next) => {

    var backupStorage=parseFloat(req.body.backupStorage);
    var backupStorageCost=backupStorage*0.021 ;

    return res.status(200).json({
        backupStorageCost
    });


};

//The following method allows you to to calculate the Aurora postgreSQL total  consumption

exports.getAuroraPosgreSQLtotal = (req, res, next) => {


    ////////////////////////////////////////Settings////////////////////////////////////
    var ACU=parseInt(req.body.ACU);
    var totalPostgreSQL=ACU*730*0.06;
    //////////////////////////////////////////////Data storage //////////////////////////
    var storageAmount=parseFloat(req.body.storageAmount);
    var storageCost=storageAmount*0.10;
    var reads=parseInt(req.body.reads);
    var writes=parseInt(req.body.writes);
    var somme =reads+writes;
    var sommePerMonth=somme*730*60*60;
    var sommeCost=sommePerMonth*0.0000002;
    var totalPostgreSQLdataStorage=sommeCost+storageCost;


    ////////////////////////////////////Backup storage //////////////////////////////////
    var backupStorage=parseFloat(req.body.backupStorage);
    var backupStorageCost=backupStorage*0.021 ;

    var total=backupStorageCost+totalPostgreSQLdataStorage+totalPostgreSQL;
    return res.status(200).json({
        total
    });


};


//////////////////////////////////////MYSQL///////////////////////////////////////////
//The following method allows you to to calculate the Aurora MySQL settings consumption

exports.getAuroraMySQLSettings = (req, res, next) => {

    var ACU=parseInt(req.body.ACU);
    var totalMySQL=ACU*730*0.06;

    return res.status(200).json({
        totalMySQL
    });


};


//The following method allows you to to calculate the Aurora MySQL data storage consumption

exports.getAuroraMySQLdataStorage = (req, res, next) => {

    var storageAmount=parseFloat(req.body.storageAmount);
    var storageCost=storageAmount*0.10;
    var reads=parseInt(req.body.reads);
    var writes=parseInt(req.body.writes);
    var somme =reads+writes;
    var sommePerMonth=somme*730*60*60;
    var sommeCost=sommePerMonth*0.0000002;
    var totalMySQLdataStorage=sommeCost+storageCost;

    return res.status(200).json({
        totalMySQLdataStorage
    });


};


//The following method allows you to to calculate the Aurora MySQL backup storage consumption

exports.getAuroraMySQLbackupStorage = (req, res, next) => {

    var backupStorage=parseFloat(req.body.backupStorage);
    var backupStorageCost=backupStorage*0.021 ;

    return res.status(200).json({
        backupStorageCost
    });


};

//The following method allows you to to calculate the Aurora MySQL total  consumption

exports.getAuroraMySQLtotal = (req, res, next) => {


    ////////////////////////////////////////Settings////////////////////////////////////
    var ACU=parseInt(req.body.ACU);
    var totalMySQL=ACU*730*0.06;
    //////////////////////////////////////////////Data storage //////////////////////////
    var storageAmount=parseFloat(req.body.storageAmount);
    var storageCost=storageAmount*0.10;
    var reads=parseInt(req.body.reads);
    var writes=parseInt(req.body.writes);
    var somme =reads+writes;
    var sommePerMonth=somme*730*60*60;
    var sommeCost=sommePerMonth*0.0000002;
    var totalMySQLdataStorage=sommeCost+storageCost;


    ////////////////////////////////////Backup storage //////////////////////////////////
    var backupStorage=parseFloat(req.body.backupStorage);
    var backupStorageCost=backupStorage*0.021 ;

    var total=backupStorageCost+totalMySQLdataStorage+totalMySQL;
    return res.status(200).json({
        total
    });


};



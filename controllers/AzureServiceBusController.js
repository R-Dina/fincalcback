const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to calculate the Azure Service Bus Basic consumption

exports.getAzureServiceBusBasic= (req, res, next) => {
    
    
    var MessangingOperations=parseInt(req.body.MessangingOperations);
    var totalAServiceBusBasic=MessangingOperations*0.05;
    

    return res.status(200).json({
                     totalAServiceBusBasic
                });
    
    
};

//The following method allows you to calculate the Azure Service Bus Standard consumption

exports.getAzureServiceBusStandard= (req, res, next) => {
    var MessangingOperations=parseInt(req.body.MessangingOperations);
    var NbListeners=parseInt(req.body.NbListeners);
    var Storage=parseInt(req.body.Storage);
    var RelayHours=parseInt(req.body.RelayHours);
    var NbMessages=parseInt(req.body.NbMessages);
    
    if (13<MessangingOperations && MessangingOperations<=100)
    {
        var diff= MessangingOperations-13;
        var TotalMsgOps=(diff*0.8)+9.81;
    }
    else if 
    (100<MessangingOperations && MessangingOperations<=2500)
    {
        var diff1= MessangingOperations-100;
        var TotalMsgOps=(diff1*0.50)+97.41;
        
    }
    else if 
    (MessangingOperations>2500)
    {
        var diff2= MessangingOperations-2500;
        var TotalMsgOps=(diff2*0.20)+1279.41;
    }
     else { 
        TotalMsgOps=9.81;
     }

    var CalculNbListeners= NbListeners*9.78;
    var TotalHybridConnect= CalculNbListeners+(Storage*1);


    if (RelayHours>=1 && RelayHours<=100)
    {
       var TotalRelayHours=0.10;
    }
    else if (RelayHours==0)
    {
        var TotalRelayHours=0;
    }
    else if
    
         (RelayHours % 100 == 0)
         {
    var x= RelayHours / 100; 
    var m=RelayHours % 100;
   var TotalRelayHours=x*0.10;}
   else 
   {
   var x= RelayHours / 100 ;
   var trunc= Math.trunc(x);
   
       TotalRelayHours=(trunc*0.10)+0.10;}

  if (NbMessages>=1 && NbMessages<=10)
    {
       var TotalNbMessages=0.01;
    }
    else if (NbMessages==0)
    {
        var TotalNbMessages=0;
    }
    else if
    
         (NbMessages % 10 == 0)
         {
    var x= NbMessages / 10; 
   
   var TotalNbMessages=x*0.01;}
   else 
   {
   var x= NbMessages / 10 ;
   var trunc= Math.trunc(x);
   
   TotalNbMessages=(trunc*0.01)+0.01;}

  var PrixTotal=TotalHybridConnect+TotalRelayHours+TotalMsgOps+TotalNbMessages;

  console.log("TotalRelayHours:"+TotalRelayHours);
  console.log("x:"+x);
  console.log("trunc:"+trunc);
  console.log("TotalHybridConnect:"+TotalHybridConnect);
  console.log("TotalMsgOps:"+TotalMsgOps);
  console.log("TotalNbMessages:"+TotalNbMessages);

    return res.status(200).json({
            PrixTotal
   });
    
    
    
};
//The following method allows you to calculate the Azure Service Bus Premium consumption
exports.getAzureServiceBusPremium= (req, res, next) => {
    
    
    var DailyMessageUnits=parseInt(req.body.DailyMessageUnits);
    var totalDailyMessageUnits=(DailyMessageUnits*0.93)*730;
    console.log("totalDailyMessageUnits:"+totalDailyMessageUnits);

    return res.status(200).json({
                totalDailyMessageUnits
                });
    
    
};
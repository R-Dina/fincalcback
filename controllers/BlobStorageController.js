const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to calculate the Blob Storage Standard LRS/ZRS (hot/cool) consumption

exports.getBlobStorageStandard = (req, res, next) => {
    var Redundancy=parseFloat(req.body.Redundancy);
    var AccessTier=parseFloat(req.body.AccessTier);
    var Capacity=parseFloat(req.body.Capacity);
    var SavingOps=parseFloat(req.body.SavingOps);
    var WriteOps=parseFloat(req.body.WriteOps);
    var ListCreateOps=parseFloat(req.body.ListCreateOps);
    var ReadOps=parseFloat(req.body.ReadOps);
    var OtherOps=parseFloat(req.body.OtherOps);
    var DataRetrivel=parseFloat(req.body.DataRetrivel);
    var DataWrite=parseFloat(req.body.DataWrite);  

    
    //LRS
    if (Redundancy==1){
        //HOT
        if (AccessTier==1){
            //PAY AS YOU GO
            if (SavingOps==1){
                if (Capacity<=51200){
                    var totalCapacity=Capacity*0.0208;
                }
                else if (Capacity>51200 && Capacity<=512000)
                {
                    var diff=Capacity-51200;
                    var totalCapacity=(51200*0.0208)+(diff*0.0200);
                }
                else if (Capacity>512000) {
                    var diff2=Capacity-512000;
                  //  var diff3=diff2-512000;
                    var totalCapacity=(51200*0.0208)+(460800*0.0200)+(diff2*0.0192);
                }
        
            }
        var totalWriteOps=(WriteOps/10000)*0.050;
        var totalListCreateOps=(ListCreateOps/10000)*0.050;
        var totalReadOps=(ReadOps/10000)*0.004;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0;
        var totalDataWrite=DataWrite*0;


        }
        //COOL
        if (AccessTier==2){
            //PAY AS YOU GO
            if (SavingOps==1){
                var totalCapacity=Capacity*0.0152;
               }

        var totalWriteOps=(WriteOps/10000)*0.100;
        var totalListCreateOps=(ListCreateOps/10000)*0.050;
        var totalReadOps=(ReadOps/10000)*0.010;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0.010;
        var totalDataWrite=DataWrite*0;

        
        }
    }

    //ZRS
    if (Redundancy==2){
        //HOT
        if (AccessTier==1){
            //PAY AS YOU GO
            if (SavingOps==1){
                if (Capacity<=51200){
                    var totalCapacity=Capacity*0.026;
                }
                else if (Capacity>51200 && Capacity<=512000)
                {
                    var diff=Capacity-51200;
                    var totalCapacity=(51200*0.026)+(diff*0.0250);
                }
                else if (Capacity>512000) {
                    var diff2=Capacity-51200;
                    var diff3=diff2-512000;
                    var totalCapacity=(51200*0.026)+(diff2*0.0250)+(diff3*0.0240);
                }
        
            }
        var totalWriteOps=(WriteOps/10000)*0.0625;
        var totalListCreateOps=(ListCreateOps/10000)*0.0625;
        var totalReadOps=(ReadOps/10000)*0.004;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0;
        var totalDataWrite=DataWrite*0;
        }
        //COOL
        if (AccessTier==2){
            //PAY AS YOU GO
            if (SavingOps==1){
                
                    var totalCapacity=Capacity*0.019;
                }

        var totalWriteOps=(WriteOps/10000)*0.10;
        var totalListCreateOps=(ListCreateOps/10000)*0.0625;
        var totalReadOps=(ReadOps/10000)*0.001;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0.01;
        var totalDataWrite=DataWrite*0;
        }   
    }
    var PrixTotal=totalDataWrite+totalDataRetrivel+totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;
    console.log("totalCapacity="+totalCapacity);
    console.log("totalWriteOps="+totalWriteOps);
    console.log("totalListCreateOps="+totalListCreateOps);
    console.log("totalReadOps="+totalReadOps);
    console.log("totalOtherOps="+totalOtherOps);
    console.log("totalDataRetrivel="+totalDataRetrivel);
    console.log("totalDataWrite="+totalDataWrite);
    return res.status(200).json({
      PrixTotal
   });
};



//The following method allows you to calculate the Blob Storage Standard LRS (Archive) consumption

exports.getBlobStorageStandardArchive= (req, res, next) => {
    var Redundancy=parseFloat(req.body.Redundancy);
    var AccessTier=parseFloat(req.body.AccessTier);
    var Capacity=parseFloat(req.body.Capacity);
    var SavingOps=parseFloat(req.body.SavingOps);
    var WriteOps=parseFloat(req.body.WriteOps);
    var ListCreateOps=parseFloat(req.body.ListCreateOps);
    var ReadOps=parseFloat(req.body.ReadOps);
    var OtherOps=parseFloat(req.body.OtherOps);
    var DataRetrivel=parseFloat(req.body.DataRetrivel);
    var DataWrite=parseFloat(req.body.DataWrite);  
    var Archivehighpriorityread=parseFloat(req.body.Archivehighpriorityread);
    var Archivehighpriorityretrieval=parseFloat(req.body.Archivehighpriorityretrieval);
//LRS
if (Redundancy==1){
    //ARCHIVE
    if (AccessTier==3){
        //PAY AS YOU GO
            if (SavingOps==1){
                var totalCapacity=Capacity*0.00099;
           }

    var totalWriteOps=(WriteOps/10000)*0.10;
    var totalListCreateOps=(ListCreateOps/10000)*0.05;
    var totalReadOps=(ReadOps/10000)*5;
    var totalArchivehighpriorityread=(Archivehighpriorityread/10000)*50;
    var totalOtherOps=(OtherOps/10000)*0.004;
    var totalDataRetrivel=DataRetrivel*0.02;
    var totalArchivehighpriorityretrieval=Archivehighpriorityretrieval*0.10;
    var totalDataWrite=DataWrite*0;

    
    }
}


var PrixTotal=totalArchivehighpriorityretrieval+totalArchivehighpriorityread+totalDataWrite+totalDataRetrivel+totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;
console.log("totalCapacity="+totalCapacity);
    console.log("totalWriteOps="+totalWriteOps);
    console.log("totalListCreateOps="+totalListCreateOps);
    console.log("totalReadOps="+totalReadOps);
    console.log("totalOtherOps="+totalOtherOps);
    console.log("totalDataRetrivel="+totalDataRetrivel);
    console.log("totalDataWrite="+totalDataWrite);
return res.status(200).json({
   PrixTotal
});
};

//The following method allows you to calculate the Blob Storage Standard GRS/RA-GRS (hot/cool) consumption
exports.getBlobStorageStandardGRS= (req, res, next) => {
    var Redundancy=parseFloat(req.body.Redundancy);
    var AccessTier=parseFloat(req.body.AccessTier);
    var Capacity=parseFloat(req.body.Capacity);
    var SavingOps=parseFloat(req.body.SavingOps);
    var WriteOps=parseFloat(req.body.WriteOps);
    var ListCreateOps=parseFloat(req.body.ListCreateOps);
    var ReadOps=parseFloat(req.body.ReadOps);
    var OtherOps=parseFloat(req.body.OtherOps);
    var DataRetrivel=parseFloat(req.body.DataRetrivel);
    var DataWrite=parseFloat(req.body.DataWrite);  
    var GeoReplicationDataTransfer=parseFloat(req.body.GeoReplicationDataTransfer);

      //GRS
      if (Redundancy==3){
        //HOT
        if (AccessTier==1){
            //PAY AS YOU GO
            if (SavingOps==1){
                if (Capacity<=51200){
                    var totalCapacity=Capacity*0.0458;
                }
                else if (Capacity>51200 && Capacity<=512000)
                {
                    var diff=Capacity-51200;
                    var totalCapacity=(51200*0.0458)+(diff*0.0440 );
                }
                else if (Capacity>512000) {
                    var diff2=Capacity-51200;
                    var totalCapacity=(51200*0.0458)+(460800*0.0440)+(diff2*0.0422);
                }
        
            }
        var totalWriteOps=(WriteOps/10000)*0.10;
        var totalListCreateOps=(ListCreateOps/10000)*0.10;
        var totalReadOps=(ReadOps/10000)*0.004;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0;
        var totalDataWrite=DataWrite*0;
        var totalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        }

        //COOL
        if (AccessTier==2){
            //PAY AS YOU GO
            if (SavingOps==1){
                
        var totalCapacity=Capacity*0.0334;   
        var totalWriteOps=(WriteOps/10000)*0.20;
        var totalListCreateOps=(ListCreateOps/10000)*0.10;
        var totalReadOps=(ReadOps/10000)*0.01;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0.01;
        var totalDataWrite=DataWrite*0.005;
        var totalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        }
     }
    
        var PrixTotal=totalGeoReplicationDataTransfer+totalDataWrite+totalDataRetrivel+totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;
    }

    //RA-GRS
    if (Redundancy==4){
        //HOT
        if (AccessTier==1){
            //PAY AS YOU GO
            if (SavingOps==1){
                if (Capacity<=51200){
                    var totalCapacity=Capacity*0.0589;
                }
                else if (Capacity>51200 && Capacity<=512000)
                {
                    var diff=Capacity-51200;
                    var totalCapacity=(51200*0.0589)+(diff*0.0566);
                }
                else if (Capacity>512000) {
                    var diff2=Capacity-51200;
                    var totalCapacity=(51200*0.0589)+(d460800iff2*0.0566)+(diff2*0.0542);
                }
        
            }
        var totalWriteOps=(WriteOps/10000)*0.10;
        var totalListCreateOps=(ListCreateOps/10000)*0.10;
        var totalReadOps=(ReadOps/10000)*0.004;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0;
        var totalDataWrite=DataWrite*0;
        var totalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        }
        //COOL
        if (AccessTier==2){
            //PAY AS YOU GO
            if (SavingOps==1){
                
        var totalCapacity=Capacity*0.0425;   
        var totalWriteOps=(WriteOps/10000)*0.20;
        var totalListCreateOps=(ListCreateOps/10000)*0.10;
        var totalReadOps=(ReadOps/10000)*0.01;
        var totalOtherOps=(OtherOps/10000)*0.004;
        var totalDataRetrivel=DataRetrivel*0.01;
        var totalDataWrite=DataWrite*0;
        var totalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        }
        }
        var PrixTotal=totalGeoReplicationDataTransfer+totalDataWrite+totalDataRetrivel+totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;

 }
 return res.status(200).json({
    PrixTotal
});

};


 //The following method allows you to calculate the Blob Storage Standard GRS/RA-GRS (Archive) consumption

exports.getBlobStorageStandardGRSArchive= (req, res, next) => {
    var Redundancy=parseFloat(req.body.Redundancy);
    var AccessTier=parseFloat(req.body.AccessTier);
    var Capacity=parseFloat(req.body.Capacity);
    var SavingOps=parseFloat(req.body.SavingOps);
    var WriteOps=parseFloat(req.body.WriteOps);
    var ListCreateOps=parseFloat(req.body.ListCreateOps);
    var ReadOps=parseFloat(req.body.ReadOps);
    var OtherOps=parseFloat(req.body.OtherOps);
    var DataRetrivel=parseFloat(req.body.DataRetrivel);
    var DataWrite=parseFloat(req.body.DataWrite);  
    var Archivehighpriorityread=parseFloat(req.body.Archivehighpriorityread);
    var Archivehighpriorityretrieval=parseFloat(req.body.Archivehighpriorityretrieval);
    var GeoReplicationDataTransfer=parseFloat(req.body.GeoReplicationDataTransfer);
//GRS
if (Redundancy==3){
    //ARCHIVE
    if (AccessTier==3){
        //PAY AS YOU GO
            if (SavingOps==1){
                var totalCapacity=Capacity*0.00299;
           }

    var totalWriteOps=(WriteOps/10000)*0.21;
    var totalListCreateOps=(ListCreateOps/10000)*0.10;
    var totalReadOps=(ReadOps/10000)*5;
    var totalArchivehighpriorityread=(Archivehighpriorityread/10000)*50;
    var totalOtherOps=(OtherOps/10000)*0.004;
    var totalDataRetrivel=DataRetrivel*0.02;
    var totalArchivehighpriorityretrieval=Archivehighpriorityretrieval*0.10;
    var totalDataWrite=DataWrite*0;
    var totalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;

    
    }
}
//RA-GRS
if (Redundancy==4){
    //ARCHIVE
    if (AccessTier==3){
        //PAY AS YOU GO
            if (SavingOps==1){
                var totalCapacity=Capacity*0.00299;
           }

    var totalWriteOps=(WriteOps/10000)*0.21;
    var totalListCreateOps=(ListCreateOps/10000)*0.10;
    var totalReadOps=(ReadOps/10000)*5;
    var totalArchivehighpriorityread=(Archivehighpriorityread/10000)*50;
    var totalOtherOps=(OtherOps/10000)*0.004;
    var totalDataRetrivel=DataRetrivel*0.02;
    var totalArchivehighpriorityretrieval=Archivehighpriorityretrieval*0.10;
    var totalDataWrite=DataWrite*0;
    var totalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;

    
    }
}


var PrixTotal=totalGeoReplicationDataTransfer+totalArchivehighpriorityretrieval+totalArchivehighpriorityread+totalDataWrite+totalDataRetrivel+totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;
console.log("totalCapacity="+totalCapacity);
console.log("totalWriteOps="+totalWriteOps);
console.log("totalListCreateOps="+totalListCreateOps);
console.log("totalReadOps="+totalReadOps);
console.log("totalOtherOps="+totalOtherOps);
console.log("totalDataRetrivel="+totalDataRetrivel);
console.log("totalDataWrite="+totalDataWrite);
console.log("totalGeoReplicationDataTransfer="+totalGeoReplicationDataTransfer);
console.log("totalArchivehighpriorityretrieval="+totalArchivehighpriorityretrieval);
console.log("totalArchivehighpriorityread="+totalArchivehighpriorityread);
return res.status(200).json({
   PrixTotal
});
};

//The following method allows you to calculate the Blob Storage Standard GZRS Hot consumption
exports.getBlobStorageStandardGzrsHot= (req, res, next) => {
    var Redundancy=parseFloat(req.body.Redundancy);
    var AccessTier=parseFloat(req.body.AccessTier);
    var Capacity=parseFloat(req.body.Capacity);
    var SavingOps=parseFloat(req.body.SavingOps);
    var WriteOps=parseFloat(req.body.WriteOps);
    var ListCreateOps=parseFloat(req.body.ListCreateOps);
    var ReadOps=parseFloat(req.body.ReadOps);
    var OtherOps=parseFloat(req.body.OtherOps);
 

//GZRS
if (Redundancy==5){
    //HOT
    if (AccessTier==1){
        //PAY AS YOU GO
        if (SavingOps==1){
            if (Capacity<=51200){
                var totalCapacity=Capacity*0.0468;
            }
            else if (Capacity>51200 && Capacity<=512000)
            {
                var diff=Capacity-51200;
                var totalCapacity=(51200*0.0468)+(diff*0.0450);
            }
            else if (Capacity>512000) {
                var diff2=Capacity-51200;
                var totalCapacity=(51200*0.0468)+(460800*0.0450)+(diff2*0.0431);
            }
    
        }
           
    var totalWriteOps=(WriteOps/10000)*0.1175;
    var totalListCreateOps=(ListCreateOps/10000)*0.1175;
    var totalReadOps=(ReadOps/10000)*0.004;
    var totalOtherOps=(OtherOps/10000)*0.004;   
    }
}


var PrixTotal=totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;
console.log("totalCapacity="+totalCapacity);
console.log("totalWriteOps="+totalWriteOps);
console.log("totalListCreateOps="+totalListCreateOps);
console.log("totalReadOps="+totalReadOps);
console.log("totalOtherOps="+totalOtherOps);

return res.status(200).json({
  PrixTotal
});
};

//The following method allows you to calculate the Blob Storage Standard GZRS Cool consumption
exports.getBlobStorageStandardGzrsCool= (req, res, next) => {
    var Redundancy=parseFloat(req.body.Redundancy);
    var AccessTier=parseFloat(req.body.AccessTier);
    var Capacity=parseFloat(req.body.Capacity);
    var SavingOps=parseFloat(req.body.SavingOps);
    var WriteOps=parseFloat(req.body.WriteOps);
    var ListCreateOps=parseFloat(req.body.ListCreateOps);
    var ReadOps=parseFloat(req.body.ReadOps);
    var OtherOps=parseFloat(req.body.OtherOps);
    var DataRetrivel=parseFloat(req.body.DataRetrivel);
 

//GZRS
if (Redundancy==5){
    //COOL
    if (AccessTier==2){
        //PAY AS YOU GO
        if (SavingOps==1){
            
                var totalCapacity=Capacity*0.0342 ;
            
        }
           
    var totalWriteOps=(WriteOps/10000)*0.20;
    var totalListCreateOps=(ListCreateOps/10000)*0.1175;
    var totalReadOps=(ReadOps/10000)*0.01;
    var totalOtherOps=(OtherOps/10000)*0.004;
    var totalDataRetrivel=DataRetrivel*0.01;   
    }
}


var PrixTotal=totalDataRetrivel+totalOtherOps+totalReadOps+totalListCreateOps+totalWriteOps+totalCapacity;
console.log("totalCapacity="+totalCapacity);
console.log("totalWriteOps="+totalWriteOps);
console.log("totalListCreateOps="+totalListCreateOps);
console.log("totalReadOps="+totalReadOps);
console.log("totalOtherOps="+totalOtherOps);

return res.status(200).json({
  PrixTotal
});
};
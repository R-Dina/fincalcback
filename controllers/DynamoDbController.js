const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to to calculate the Data Storage of Amazon DynamoDB on-demand capacity consumption

exports.getDynamoDBOnDemandCapacityDataStorage= (req, res, next) => {

    var dataStorageSize=parseInt(req.body.dataStorageSize);
    var itemSize=parseInt(req.body.itemSize);
    var totalDataStorage=dataStorageSize*0.25;
    return res.status(200).json({
        totalDataStorage
    });


};

//The following method allows you to to calculate the Write Settings of Amazon DynamoDB on-demand capacity consumption

exports.getDynamoDBOnDemandCapacityWriteSettings= (req, res, next) => {
    var standardWrites=parseInt(req.body.standardWrites);
    var transacionalWrites=parseInt(req.body.transacionalWrites);
    var standWritesPour=standardWrites/100;
    var transactiWritesPour=transacionalWrites/100;
    var numOfWrites=parseInt(req.body.numOfWrites);
    var numberOfWritesPerMillion=numOfWrites*1000000;
    var itemSize=parseInt(req.body.itemSize);
    var averageItemSize=Math.round(itemSize);
    var total1=numberOfWritesPerMillion*standWritesPour*1*averageItemSize;
    var total2=numberOfWritesPerMillion*transactiWritesPour*2*averageItemSize;
    var total3=total1+total2;
    var totalWriteSettings=total3*0.00000125;
    return res.status(200).json({
        totalWriteSettings
    });
};

//The following method allows you to to calculate the Read Settings of Amazon DynamoDB on-demand capacity consumption

exports.getDynamoDBOnDemandCapacityReadSettings= (req, res, next) => {

    var eventuallyConsistent=parseInt(req.body.eventuallyConsistent);
    var stronglyConsistent=parseInt(req.body.stronglyConsistent);
    var transactional=parseInt(req.body.transactional);
    var eventuallyConsistentP=eventuallyConsistent/100;
    var stronglyConsistentP=stronglyConsistent/100;
    var transactionalP=transactional/100;
    var numReads=parseInt(req.body.numReads);
    var itemSize=parseInt(req.body.itemSize);
    var averageItemSize=Math.round(itemSize/4);
    var total1=numReads*1000000*eventuallyConsistentP*0.5*averageItemSize;
    var total2=numReads*1000000*stronglyConsistentP*1*averageItemSize;
    var total3=numReads*1000000*transactionalP*2*averageItemSize;
    var totalOnDemandReadCost=(total1+total2+total3)*0.00000025;

    return res.status(200).json({
        totalOnDemandReadCost
    });


};

//The following method allows you to to calculate the total of Amazon DynamoDB on-demand capacity consumption

exports.getDynamoDBOnDemandCapacityTotal= (req, res, next) => {

    ///////////////////////////////////////Data Storage////////////////////////////////////
    var dataStorageSize=parseInt(req.body.dataStorageSize);
    var itemSize=parseInt(req.body.itemSize);
    var totalDataStorage=dataStorageSize*0.25;

    ///////////////////////////////////////Write Settings////////////////////////////////////

    var standardWrites=parseInt(req.body.standardWrites);
    var transacionalWrites=parseInt(req.body.transacionalWrites);
    var standWritesPour=standardWrites/100;
    var transactiWritesPour=transacionalWrites/100;
    var numOfWrites=parseInt(req.body.numOfWrites);
    var numberOfWritesPerMillion=numOfWrites*1000000;
    var averageItemSize=Math.round(itemSize);
    var total1=numberOfWritesPerMillion*standWritesPour*1*averageItemSize;
    var total2=numberOfWritesPerMillion*transactiWritesPour*2*averageItemSize;
    var total3=total1+total2;
    var totalWriteSettings=total3*0.00000125;

    /////////////////////////////////////Read Settings /////////////////////////////////////

    var eventuallyConsistent=parseInt(req.body.eventuallyConsistent);
    var stronglyConsistent=parseInt(req.body.stronglyConsistent);
    var transactional=parseInt(req.body.transactional);
    var eventuallyConsistentP=eventuallyConsistent/100;
    var stronglyConsistentP=stronglyConsistent/100;
    var transactionalP=transactional/100;
    var numReads=parseInt(req.body.numReads);
    var averageItemSize1=Math.round(itemSize/4);
    var total11=numReads*1000000*eventuallyConsistentP*0.5*averageItemSize1;
    var total22=numReads*1000000*stronglyConsistentP*1*averageItemSize1;
    var total33=numReads*1000000*transactionalP*2*averageItemSize1;
    var totalOnDemandReadCost=(total11+total22+total33)*0.00000025;
    var totalDynamoOnDemandCap=totalDataStorage+totalWriteSettings+totalOnDemandReadCost;

    return res.status(200).json({
        totalDynamoOnDemandCap
    });

};

//The following method allows you to to calculate the Data Storage of Amazon DynamoDB provisioned capacity consumption

exports.getDynamoDBProvisionedCapacityDataStorage= (req, res, next) => {
    var dataStorageSize1=parseInt(req.body.dataStorageSize1);
    var itemSize1=parseInt(req.body.itemSize1);
    var totalDataStorage=dataStorageSize1*0.25;
    return res.status(200).json({
        totalDataStorage
    });
};

//The following method allows you to to calculate the Write settings of Amazon DynamoDB provisioned capacity consumption

exports.getDynamoDBProvisionedCapacityWriteSettings= (req, res, next) => {

    var standardWrites1=parseInt(req.body.standardWrites1);
    var transacionalWrites1=parseInt(req.body.transacionalWrites1);
    var baselineWrites=parseInt(req.body.baselineWrites);
    var peakWrites=parseInt(req.body.peakWrites);
    var durationPeakWriteActivity=parseInt(req.body.durationPeakWriteActivity);
    var percentageOfBaselineWrites=parseInt(req.body.percentageOfBaselineWrites);
    var standWritesPour=standardWrites1/100;
    var transactiWritesPour=transacionalWrites1/100;
    var baselineWritesPour=percentageOfBaselineWrites/100;
    var itemSize1=parseInt(req.body.itemSize1);
    var averageItemSize=Math.round(itemSize1);
    var A=baselineWrites*standWritesPour*averageItemSize;
    var B=baselineWrites*transactiWritesPour*2*averageItemSize;
    var C=A+B;
    var D=C*baselineWritesPour;
    var E=parseFloat(Math.ceil(D/100));
    var F=E*100;
    var G=C-F;
    var H=Math.max(G,0);
    var I=Math.round(H);
    var J=730-durationPeakWriteActivity;
    var K=I*J;
    var L=Math.round(K);
    var M=peakWrites*standWritesPour*averageItemSize;
    var N=peakWrites*transactiWritesPour*2*averageItemSize;
    var O=M+N;
    var P=O-F;
    var R=Math.max(P,0);
    var S=Math.round(R);
    var T=S*durationPeakWriteActivity;
    var U=T+K;
    var V=Math.round(U);
    var X=V*0.00065;
    var Y=F*0.000128*730;
    var totalWriteSettingsProvisioned=X+Y;
    var upFrontWriteCost=F*1.50;

    return res.status(200).json({
       totalWriteSettingsProvisioned , upFrontWriteCost
    });


};

//The following method allows you to to calculate the Read settings of Amazon DynamoDB provisioned capacity consumption

exports.getDynamoDBProvisionedCapacityReadSettings= (req, res, next) => {
  console.log("im in");
    var eventuallyConsistent1=parseInt(req.body.eventuallyConsistent1);
    var stronglyConsistent1=parseInt(req.body.stronglyConsistent1);
    var transactional1=parseInt(req.body.transactional1);
    var baselineReadRate=parseInt(req.body.baselineReadRate);
    var peakReadRate=parseInt(req.body.peakReadRate);
    var durationOfPeakReadActivity=parseInt(req.body.durationOfPeakReadActivity);
    var percentageOfBaselineReads=parseInt(req.body.PercentageOfBaselineReads);
    var eventuallyConsistentPour=eventuallyConsistent1/100;
    var stronglyConsistentPour=stronglyConsistent1/100;
    var transactionalPour=transactional1/100;
    var percentageOfBaselineReadsPour=percentageOfBaselineReads/100;
    var itemSize1=(parseInt(req.body.itemSize1))/4;
    var averageItemSize=Math.ceil(itemSize1);
    var A=baselineReadRate*eventuallyConsistentPour*0.5*averageItemSize;

    var B=baselineReadRate*stronglyConsistentPour*1*averageItemSize;
    var C=baselineReadRate*transactionalPour*2*averageItemSize;
    var D=A+B+C;
    var E=D*percentageOfBaselineReadsPour;
    var F=E/100;
    var G=parseFloat(Math.ceil(F));
    var H=G*100;
    var I=D-H;
    var J=Math.max(I,0);
    var K=Math.round(J);
    var L=730-durationOfPeakReadActivity;
    var M=L*I;
    var N=peakReadRate*eventuallyConsistentPour*0.5*averageItemSize;
    var O=peakReadRate*stronglyConsistentPour*1*averageItemSize;
    var P=peakReadRate*transactionalPour*2*averageItemSize;
    var R=N+O+P;
    var S=R-H;
    var T=Math.max(S,0);
    var U=Math.round(T);
    var V=U*durationOfPeakReadActivity;
    var X=M+V;
    var firstPrice=X*0.00013;
    var secondPrice=H*0.000025*730;
    var totalReadSettingsProvisioned=firstPrice+secondPrice;
    var upFrontReadCost=H*0.30;

  console.log("totalReadSettingsProvisioned "+totalReadSettingsProvisioned);

  return res.status(200).json({
       totalReadSettingsProvisioned ,
          upFrontReadCost
    });

};

//The following method allows you to to calculate the total consumption of Amazon DynamoDB provisioned concurrency

exports.getDynamoDBProvisionedCapacityTotal = (req, res, next) => {
    //////////////////////////////////Data storage//////////////////////////
    var dataStorageSize1=parseInt(req.body.dataStorageSize1);
    var itemSize1=parseInt(req.body.itemSize1);
    var totalDataStorage=dataStorageSize1*0.25;

    ///////////////////////////////////////Write Settings/////////////////////
    var standardWrites1=parseInt(req.body.standardWrites1);
    var transacionalWrites1=parseInt(req.body.transacionalWrites1);
    var baselineWrites=parseInt(req.body.baselineWrites);
    var peakWrites=parseInt(req.body.peakWrites);
    var durationPeakWriteActivity=parseInt(req.body.durationPeakWriteActivity);
    var percentageOfBaselineWrites=parseInt(req.body.percentageOfBaselineWrites);
    var standWritesPour=standardWrites1/100;
    var transactiWritesPour=transacionalWrites1/100;
    var baselineWritesPour=percentageOfBaselineWrites/100;
    var averageItemSize=Math.round(itemSize1);
    var A=baselineWrites*standWritesPour*averageItemSize;
    var B=baselineWrites*transactiWritesPour*2*averageItemSize;
    var C=A+B;
    var D=C*baselineWritesPour;
    var E=parseFloat(Math.ceil(D/100));
    var F=E*100;
    var G=C-F;
    var H=Math.max(G,0);
    var I=Math.round(H);
    var J=730-durationPeakWriteActivity;
    var K=I*J;
    var L=Math.round(K);
    var M=peakWrites*standWritesPour*averageItemSize;
    var N=peakWrites*transactiWritesPour*2*averageItemSize;
    var O=M+N;
    var P=O-F;
    var R=Math.max(P,0);
    var S=Math.round(R);
    var T=S*durationPeakWriteActivity;
    var U=T+K;
    var V=Math.round(U);
    var X=V*0.00065;
    var Y=F*0.000128*730;
    var totalWriteSettingsProvisioned=X+Y;
    var upFrontWriteCost=F*1.50;

    /////////////////////////////////////////Read Settings//////////////////////////

    var eventuallyConsistent1=parseInt(req.body.eventuallyConsistent1);
    var stronglyConsistent1=parseInt(req.body.stronglyConsistent1);
    var transactional1=parseInt(req.body.transactional1);
    var baselineReadRate=parseInt(req.body.baselineReadRate);
    var peakReadRate=parseInt(req.body.peakReadRate);
    var durationOfPeakReadActivity=parseInt(req.body.durationOfPeakReadActivity);
    var percentageOfBaselineReads=parseInt(req.body.PercentageOfBaselineReads);
    var eventuallyConsistentPour=eventuallyConsistent1/100;
    var stronglyConsistentPour=stronglyConsistent1/100;
    var transactionalPour=transactional1/100;
    var percentageOfBaselineReadsPour=percentageOfBaselineReads/100;
    var itemSize2=itemSize1/4;
    var averageItemSize1=Math.ceil(itemSize2);
    var A1=baselineReadRate*eventuallyConsistentPour*0.5*averageItemSize1;

    var B1=baselineReadRate*stronglyConsistentPour*1*averageItemSize1;
    var C1=baselineReadRate*transactionalPour*2*averageItemSize1;
    var D1=A1+B1+C1;
    var E1=D1*percentageOfBaselineReadsPour;
    var F1=E1/100;
    var G1=parseFloat(Math.ceil(F1));
    var H1=G1*100;
    var I1=D1-H1;
    var J1=Math.max(I1,0);
    var K1=Math.round(J1);
    var L1=730-durationOfPeakReadActivity;
    var M1=L1*I1;
    var N1=peakReadRate*eventuallyConsistentPour*0.5*averageItemSize1;
    var O1=peakReadRate*stronglyConsistentPour*1*averageItemSize1;
    var P1=peakReadRate*transactionalPour*2*averageItemSize1;
    var R1=N1+O1+P1;
    var S1=R1-H1;
    var T1=Math.max(S1,0);
    var U1=Math.round(T1);
    var V1=U1*durationOfPeakReadActivity;
    var X1=M1+V1;
    var firstPrice=X1*0.00013;
    var secondPrice=H1*0.000025*730;
    var totalReadSettingsProvisioned=firstPrice+secondPrice;
    var upFrontReadCost=H1*0.30;
    var upFrontTotalCost=upFrontWriteCost+upFrontReadCost;
    var totalProvisonedConcurrencyDynamoDB=totalReadSettingsProvisioned+totalWriteSettingsProvisioned+totalDataStorage;
    return res.status(200).json({
       totalProvisonedConcurrencyDynamoDB ,
       upFrontWriteCost ,
       upFrontReadCost ,
       upFrontTotalCost
    });

};

//The following method allows you to to calculate the total consumption of Amazon DynamoDB

exports.getDynamoDBTotal = (req, res, next) => {
    ////////////////////////////////////////On-demand//////////////////////////////////////

    ///////////////////////////////////////Data Storage////////////////////////////////////
    var dataStorageSize=parseInt(req.body.dataStorageSize);
    var itemSize=parseInt(req.body.itemSize);
    var totalDataStorage=dataStorageSize*0.25;

    ///////////////////////////////////////Write Settings////////////////////////////////////

    var standardWrites=parseInt(req.body.standardWrites);
    var transacionalWrites=parseInt(req.body.transacionalWrites);
    var standWritesPour=standardWrites/100;
    var transactiWritesPour=transacionalWrites/100;
    var numOfWrites=parseInt(req.body.numOfWrites);
    var numberOfWritesPerMillion=numOfWrites*1000000;
    var averageItemSize=Math.round(itemSize);
    var total1=numberOfWritesPerMillion*standWritesPour*1*averageItemSize;
    var total2=numberOfWritesPerMillion*transactiWritesPour*2*averageItemSize;
    var total3=total1+total2;
    var totalWriteSettings=total3*0.00000125;

    /////////////////////////////////////Read Settings /////////////////////////////////////

    var eventuallyConsistent=parseInt(req.body.eventuallyConsistent);
    var stronglyConsistent=parseInt(req.body.stronglyConsistent);
    var transactional=parseInt(req.body.transactional);
    var eventuallyConsistentP=eventuallyConsistent/100;
    var stronglyConsistentP=stronglyConsistent/100;
    var transactionalP=transactional/100;
    var numReads=parseInt(req.body.numReads);
    var averageItemSize1=Math.ceil(itemSize/4);
    var total11=numReads*1000000*eventuallyConsistentP*0.5*averageItemSize1;
    var total22=numReads*1000000*stronglyConsistentP*1*averageItemSize1;
    var total33=numReads*1000000*transactionalP*2*averageItemSize1;
    var totalOnDemandReadCost=(total11+total22+total33)*0.00000025;

    var totalDynamoOnDemandCap=totalDataStorage+totalWriteSettings+totalOnDemandReadCost;

    ////////////////////////////////Provisioned//////////////////////////////////////////

    //////////////////////////////////Data storage//////////////////////////
    var dataStorageSize1=parseInt(req.body.dataStorageSize1);
    var itemSize1=parseInt(req.body.itemSize1);
    var totalDataStorage1=dataStorageSize1*0.25;
    ///////////////////////////////////////Write Settings/////////////////////
    var standardWrites1=parseInt(req.body.standardWrites1);
    var transacionalWrites1=parseInt(req.body.transacionalWrites1);
    var baselineWrites=parseInt(req.body.baselineWrites);
    var peakWrites=parseInt(req.body.peakWrites);
    var durationPeakWriteActivity=parseInt(req.body.durationPeakWriteActivity);
    var percentageOfBaselineWrites=parseInt(req.body.percentageOfBaselineWrites);
    var standWritesPour1=standardWrites1/100;
    var transactiWritesPour1=transacionalWrites1/100;
    var baselineWritesPour=percentageOfBaselineWrites/100;
    var averageItemSize2=Math.round(itemSize1);
    var A=baselineWrites*standWritesPour1*averageItemSize2;
    var B=baselineWrites*transactiWritesPour1*2*averageItemSize2;
    var C=A+B;
    var D=C*baselineWritesPour;
    var E=parseFloat(Math.ceil(D/100));
    var F=E*100;
    var G=C-F;
    var H=Math.max(G,0);
    var I=Math.round(H);
    var J=730-durationPeakWriteActivity;
    var K=I*J;
    var L=Math.round(K);
    var M=peakWrites*standWritesPour1*averageItemSize2;
    var N=peakWrites*transactiWritesPour1*2*averageItemSize2;
    var O=M+N;
    var P=O-F;
    var R=Math.max(P,0);
    var S=Math.round(R);
    var T=S*durationPeakWriteActivity;
    var U=T+K;
    var V=Math.round(U);
    var X=V*0.00065;
    var Y=F*0.000128*730;
    var totalWriteSettingsProvisioned=X+Y;
    var upFrontWriteCost=F*1.50;
    /////////////////////////////////////////Read Settings//////////////////////////

    var eventuallyConsistent1=parseInt(req.body.eventuallyConsistent1);
    var stronglyConsistent1=parseInt(req.body.stronglyConsistent1);
    var transactional1=parseInt(req.body.transactional1);
    var baselineReadRate=parseInt(req.body.baselineReadRate);
    var peakReadRate=parseInt(req.body.peakReadRate);
    var durationOfPeakReadActivity=parseInt(req.body.durationOfPeakReadActivity);
    var percentageOfBaselineReads=parseInt(req.body.PercentageOfBaselineReads);
    var eventuallyConsistentPour=eventuallyConsistent1/100;
    var stronglyConsistentPour=stronglyConsistent1/100;
    var transactionalPour=transactional1/100;
    var percentageOfBaselineReadsPour=percentageOfBaselineReads/100;
    var itemSize3=itemSize1/4;
    var averageItemSize3=Math.ceil(itemSize3);
    var A1=baselineReadRate*eventuallyConsistentPour*0.5*averageItemSize3;

    var B1=baselineReadRate*stronglyConsistentPour*1*averageItemSize3;
    var C1=baselineReadRate*transactionalPour*2*averageItemSize3;
    var D1=A1+B1+C1;
    var E1=D1*percentageOfBaselineReadsPour;
    var F1=E1/100;
    var G1=parseFloat(Math.ceil(F1));
    var H1=G1*100;
    var I1=D1-H1;
    var J1=Math.max(I1,0);
    var K1=Math.round(J1);
    var L1=730-durationOfPeakReadActivity;
    var M1=L1*J1;
    var N1=peakReadRate*eventuallyConsistentPour*0.5*averageItemSize3;

    var O1=peakReadRate*stronglyConsistentPour*1*averageItemSize3;
    var P1=peakReadRate*transactionalPour*2*averageItemSize3;
    var R1=N1+O1+P1;
    var S1=R1-H1;
    var T1=Math.max(S1,0);
    var U1=Math.round(T1);
    var V1=U1*durationOfPeakReadActivity;
    var X1=M1+V1;
    var firstPrice=X1*0.00013;
    var secondPrice=H1*0.000025*730;
    var totalReadSettingsProvisioned=firstPrice+secondPrice;

    var upFrontReadCost=H1*0.30;
    var totalProvisonedConcurrencyDynamoDB=totalReadSettingsProvisioned+totalWriteSettingsProvisioned+totalDataStorage1;

    var totalDynamoDB=totalProvisonedConcurrencyDynamoDB+totalDynamoOnDemandCap;
    var upFrontCost=upFrontWriteCost+upFrontReadCost;
    return res.status(200).json({
       totalDynamoDB,
         upFrontCost
    });
};

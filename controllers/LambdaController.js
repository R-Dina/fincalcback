const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to to calculate the basic lambda consumption

exports.getLambdaBasic = (req, res, next) => {
    var numRequests=parseInt(req.body.numRequests);
    var durationRequest=parseFloat(req.body.durationRequest);
    var memory=parseFloat(req.body.memory);
    var total= numRequests*durationRequest*memory;
    var diff1= total-400000;
    var prixCompute;
    var prixReq;
    if (diff1>0)
    {
        prixCompute=diff1*0.0000166667;
    }else {
        prixCompute=0;
    }
    var diff2=numRequests-1000000;
    if (diff2>0)
    {
        prixReq=diff2*0.0000002;
    }else {
        prixReq=0;
    }
    var totalBasicLambda=prixCompute+prixReq;
    return res.status(200).json({
                     "totalBasicLambda":totalBasicLambda
                });

};

//The following method allows you to to calculate the  lambda  provisioned concurrency consumption


exports.getLambdaProvConcurrency = (req, res, next) => {

    var numRequests=parseInt(req.body.numRequests);
    var durationRequest=parseFloat(req.body.durationRequest);
    var memory=parseFloat(req.body.memory);
    var concurrency=parseInt(req.body.concurrency);
    var timeConcurrency=parseInt(req.body.timeConcurrency);
    var total1= concurrency*timeConcurrency*3600*memory*0.0000041667;
    var time= durationRequest*numRequests;
  console.log("time "+time);
    var timeMemory=time*memory;
  console.log("timeMemory "+timeMemory);
    var total2=timeMemory*0.0000097222;
    console.log("total1 "+total1);
    console.log("total2 "+total2);
    var total3=numRequests*0.0000002;
  console.log("total3 "+total3);
    var totalProvLambda=total1+total2+total3;
    return res.status(200).json({
        totalProvLambda
    });

};

//The following method allows you to to calculate the total lambda consumption

exports.getLambdaTotal = (req, res, next) => {
    var numRequests=parseInt(req.body.numRequests);
    var durationRequest=parseFloat(req.body.durationRequest);
    var memory=parseInt(req.body.memory);
    var total= numRequests*durationRequest*memory;
    var diff1= total-400000;
    var prixCompute;
    var prixReq;

    if (diff1>0)
    {
        prixCompute=diff1*0.0000166667;
    }else {
        prixCompute=0;
    }
    var diff2=numRequests-1000000;
    if (diff2>0)
    {
        prixReq=diff2*0.0000002;
    }else {
        prixReq=0;
    }
    var totalBasicLambda=prixCompute+prixReq;

    var numRequests1=parseInt(req.body.numRequests1);
    var durationRequest1=parseFloat(req.body.durationRequest1);
    var memory1=parseInt(req.body.memory1);
    var concurrency=parseInt(req.body.concurrency);
    var timeConcurrency=parseInt(req.body.timeConcurrency);
    var total1= concurrency*timeConcurrency*3600*memory1*0.0000041667;
    var time= durationRequest1*numRequests1;
    var timeMemory=time*memory1;
    var total2=timeMemory*0.0000097222;
    var total3=numRequests1*0.0000002;
    var totalProvLambda=total1+total2+total3;
    var totalLambda=totalBasicLambda+totalProvLambda;
    return res.status(200).json({
        totalLambda
    });
};

const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to calculate the Queue Storage v1 consumption

exports.getQueueStorageV1 = (req, res, next) => {
    var capacity=parseFloat(req.body.capacity);
    var nbQueueOperationsClass1=parseInt(req.body.nbQueueOperationsClass1);
    var nbQueueOperationsClass2=parseInt(req.body.nbQueueOperationsClass2);
    var redundancy=parseInt(req.body.redundancy);
    if (redundancy==1){
        var TotalCapacity=capacity*0.0450;
    }
    if (redundancy==2){
        var TotalCapacity=capacity*0.0600;
    }

    if (redundancy==3){
        var TotalCapacity=capacity*0.0750;
    }
    var TotalNbQueueOpsClass1=nbQueueOperationsClass1*0.0004;
    var TotalNbQueueOpsClass2=nbQueueOperationsClass2*0.0004;
    var PrixTotal=TotalNbQueueOpsClass1+TotalNbQueueOpsClass2+TotalCapacity;
    var arrond= Math.round(PrixTotal);                                                                                                                                  
    console.log("arrond="+arrond);


    return res.status(200).json({
       PrixTotal
   });


    
};

//The following method allows you to calculate the Queue Storage v2 consumption

exports.getQueueStorageV2 = (req, res, next) => {
    var capacity=parseFloat(req.body.capacity);
    var nbQueueOperationsClass1=parseInt(req.body.nbQueueOperationsClass1);
    var nbQueueOperationsClass2=parseInt(req.body.nbQueueOperationsClass2);
    var redundancy=parseInt(req.body.redundancy);
    var GeoReplicationDataTransfer=parseInt(req.body.GeoReplicationDataTransfer);

    if (redundancy==1){
        var TotalCapacity=capacity*0.0450;
        var TotalNbQueueOpsClass1=nbQueueOperationsClass1*0.0040;
        var TotalNbQueueOpsClass2=nbQueueOperationsClass2*0.0040;
        var PrixTotal=TotalNbQueueOpsClass1+TotalNbQueueOpsClass2+TotalCapacity;
    }
    if (redundancy==2){
        var TotalCapacity=capacity*0.0563;
        var TotalNbQueueOpsClass1=nbQueueOperationsClass1*0.0040;
        var TotalNbQueueOpsClass2=nbQueueOperationsClass2*0.0040;
        var PrixTotal=TotalNbQueueOpsClass1+TotalNbQueueOpsClass2+TotalCapacity;
    }

    if (redundancy==3){
        var TotalCapacity=capacity*0.0600;
        var TotalNbQueueOpsClass1=nbQueueOperationsClass1*0.0080;
        var TotalNbQueueOpsClass2=nbQueueOperationsClass2*0.0040;
        var TotalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        var PrixTotal=TotalNbQueueOpsClass1+TotalNbQueueOpsClass2+TotalCapacity+TotalGeoReplicationDataTransfer;

        
    }
    if (redundancy==4){
        var TotalCapacity=capacity*0.0750;
        var TotalNbQueueOpsClass1=nbQueueOperationsClass1*0.0080;
        var TotalNbQueueOpsClass2=nbQueueOperationsClass2*0.0040;
        var TotalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        var PrixTotal=TotalNbQueueOpsClass1+TotalNbQueueOpsClass2+TotalCapacity+TotalGeoReplicationDataTransfer;

        

    }
    if (redundancy==5){
        var TotalCapacity=capacity*0.1013;
        var TotalNbQueueOpsClass2=nbQueueOperationsClass2*0.0080;
        var TotalGeoReplicationDataTransfer=GeoReplicationDataTransfer*0.020;
        var PrixTotal=TotalNbQueueOpsClass2+TotalCapacity+TotalGeoReplicationDataTransfer;

    }

  
    var arrond= Math.round(PrixTotal);     
    console.log("TotalCapacity="+TotalCapacity);
    console.log("TotalNbQueueOpsClass1="+TotalNbQueueOpsClass1);
    console.log("TotalNbQueueOpsClass2="+TotalNbQueueOpsClass2);                                                                                                                           
    console.log("PrixTotal="+PrixTotal);


    return res.status(200).json({
   PrixTotal
   });


    
};
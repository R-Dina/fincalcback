const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to to calculate the Standard Amazon S3 consumption

exports.getS3Standard= (req, res, next) => {
    var storage=parseFloat(req.body.storage);
    var putReq=parseInt(req.body.putReq);
    var getReq=parseInt(req.body.getReq);
    var dataReturned=parseInt(req.body.dataReturned);
    var dataScanned=parseInt(req.body.dataScanned);
    var costStorage=storage*0.0230000000;
    var costPutReq=putReq*0.000005;
    var costGetReq=getReq*0.0000004;
    var costDataReturned=dataReturned*0.0007;
    var costDataScanned=dataScanned*0.002 ;
    var total=costStorage+costPutReq+costGetReq+costDataReturned+costDataScanned;
    return res.status(200).json({
        total
    });


};

//The following method allows you to to calculate the Glacier Amazon S3 consumption

exports.getS3Glacier= (req, res, next) => {
    var storage1=parseInt(req.body.storage1);
    var putRequest=parseInt(req.body.putRequest);
    var lifecycle=parseInt(req.body.lifecycle);
    var restoreReqStandard=parseInt(req.body.restoreReqStandard);
    var restoreReqExp=parseInt(req.body.restoreReqExp);
    var restoreReqBulk=parseInt(req.body.restoreReqBulk);
    var dataRetrivStand=parseInt(req.body.dataRetrivStand);
    var dataRetrivExp=parseInt(req.body.dataRetrivExp);
    var dataRetrivBulk=parseInt(req.body.dataRetrivBulk);
    var provisionedCapUint=parseInt(req.body.provisionedCapUint);

    var storageCost=storage1*0.004;
    var putRequestCost=putRequest*0.00003;
    var lifecycleCost=lifecycle*0.00003;
    var restoreReqStandardCost=restoreReqStandard*0.00003;
    var restoreReqExpCost=restoreReqExp*0.01;
    var restoreReqBulkCost=restoreReqBulk*0.000025;
    var dataRetrivStandCost=dataRetrivStand*0.01;
    var dataRetrivExpCost=dataRetrivExp*0.03;
    var dataRetrivBulkCost=dataRetrivBulk*0.0025;
    var provisionedCapUintCost=provisionedCapUint*100.00;
    var total1=storageCost+putRequestCost+lifecycleCost+restoreReqStandardCost+restoreReqExpCost+restoreReqBulkCost+
        dataRetrivStandCost+dataRetrivExpCost+dataRetrivBulkCost+provisionedCapUintCost;

    return res.status(200).json({
        total1
    });
};

//The following method allows you to to calculate the total Amazon S3 consumption

exports.getS3Total= (req, res, next) => {

    //S3 Standard
    var storage=parseFloat(req.body.storage);
    var putReq=parseInt(req.body.putReq);
    var getReq=parseInt(req.body.getReq);
    var dataReturned=parseInt(req.body.dataReturned);
    var dataScanned=parseInt(req.body.dataScanned);
    var costStorage=storage*0.0230000000;
    var costPutReq=putReq*0.000005;
    var costGetReq=getReq*0.0000004;
    var costDataReturned=dataReturned*0.0007;
    var costDataScanned=dataScanned*0.002 ;
    var total=costStorage+costPutReq+costGetReq+costDataReturned+costDataScanned;

    //S3 GLACIER
    var storage1=parseInt(req.body.storage1);
    var putRequest=parseInt(req.body.putRequest);
    var lifecycle=parseInt(req.body.lifecycle);
    var restoreReqStandard=parseInt(req.body.restoreReqStandard);
    var restoreReqExp=parseInt(req.body.restoreReqExp);
    var restoreReqBulk=parseInt(req.body.restoreReqBulk);
    var dataRetrivStand=parseInt(req.body.dataRetrivStand);
    var dataRetrivExp=parseInt(req.body.dataRetrivExp);
    var dataRetrivBulk=parseInt(req.body.dataRetrivBulk);
    var provisionedCapUint=parseInt(req.body.provisionedCapUint);

    var storageCost=storage1*0.004;
    var putRequestCost=putRequest*0.00003;
    var lifecycleCost=lifecycle*0.00003;
    var restoreReqStandardCost=restoreReqStandard*0.00003;
    var restoreReqExpCost=restoreReqExp*0.01;
    var restoreReqBulkCost=restoreReqBulk*0.000025;
    var dataRetrivStandCost=dataRetrivStand*0.01;
    var dataRetrivExpCost=dataRetrivExp*0.03;
    var dataRetrivBulkCost=dataRetrivBulk*0.0025;
    var provisionedCapUintCost=provisionedCapUint*100.00;
    var total1=storageCost+putRequestCost+lifecycleCost+restoreReqStandardCost+restoreReqExpCost+restoreReqBulkCost+
        dataRetrivStandCost+dataRetrivExpCost+dataRetrivBulkCost+provisionedCapUintCost;

    var totalS3=total1+total;

    return res.status(200).json({
        totalS3
    });

};



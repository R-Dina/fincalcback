const express = require("express");
var session = require('express-session');



var app=express();

//The following method allows you to to calculate the service settings of  Amazon SNS consumption


exports.getSnsServiceSettings = (req, res, next) => {

    var request=parseInt(req.body.request);
    var httpNotif=parseInt(req.body.HttpNotif);
    var emailNotif=parseInt(req.body.emailNotif);
    var sqsNotif=parseInt(req.body.sqsNotif);
    var requestCost=request*0.0000005*1000000;
    var httpNotifCost=httpNotif*0.0000006*1000000;
    var emailNotifCost=emailNotif*0.00002*1000000;
    var sqsNotifCost=sqsNotif*0.00*1000000;
    var total=requestCost+httpNotifCost+emailNotifCost+sqsNotifCost;

    return res.status(200).json({
        total
    });


};


//The following method allows you to to calculate the additional notifications of  Amazon SNS consumption

exports.getSnsAdditionalNotif = (req, res, next) => {

    var mobilePushNotif=parseInt(req.body.mobilePushNotif);
    var mobilePushNotifCost=mobilePushNotif*0.0000005*1000000;
    var totalRounded=Math.round(mobilePushNotifCost);

    return res.status(200).json({
        totalRounded
    });
};

//The following method allows you to to calculate the data transfer of  Amazon SNS consumption

exports.getSnsDataTransfer = (req, res, next) => {

    var inBoundData=parseInt(req.body.inBoundData);
    var inBoundDataPrice=inBoundData*0;
    var outBoundDataType=parseInt(req.body.outBoundDataType);
    var outBoundDataAmount=parseInt(req.body.outBoundDataAmount);
    var outBoundDataCost;
    var outBoundDataCost1;
    var outBoundDataCost2;
    var outBoundDataCost3;
    var outBoundDataCost4;
    var total;
    if(outBoundDataType==1) {
        if (outBoundDataAmount == 0) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount == 1) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount <= 10239) {

            outBoundDataCost = (outBoundDataAmount - 1) * 0.09;
        }

        if (outBoundDataAmount > 10239 && outBoundDataAmount <= 51200) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = (outBoundDataAmount - 10240) * 0.085;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2;
        }

        if (outBoundDataAmount >= 51201) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = (outBoundDataAmount - (51199)) * 0.07;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3;
        }
        if (outBoundDataAmount >= 153600) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = 102400 * 0.07;
            outBoundDataCost4 = (outBoundDataAmount - (153599)) * 0.05;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3 + outBoundDataCost4;
        }
    }else if (outBoundDataType==2) {

        outBoundDataCost=0;

    }else if(outBoundDataType==3 || outBoundDataType==4 || outBoundDataType==5 || outBoundDataType==6 || outBoundDataType==7 || outBoundDataType==8 || outBoundDataType==9) {

        outBoundDataCost=outBoundDataAmount*0.01;
    }
    else if(outBoundDataType==10) {

        outBoundDataCost=outBoundDataAmount*0.02;
    }
    total=outBoundDataCost+inBoundDataPrice;
        return res.status(200).json({
            total
        });
};

//The following method allows you to to calculate the total  Amazon SNS consumption

exports.getSnsTotal = (req, res, next) => {



    //////////////////////////////Service Settings///////////////////////////
    var request=parseInt(req.body.request);
    var httpNotif=parseInt(req.body.HttpNotif);
    var emailNotif=parseInt(req.body.emailNotif);
    var sqsNotif=parseInt(req.body.sqsNotif);
    var requestCost=request*0.0000005*1000000;
    var httpNotifCost=httpNotif*0.0000006*1000000;
    var emailNotifCost=emailNotif*0.00002*1000000;
    var sqsNotifCost=sqsNotif*0.00*1000000;
    var total=requestCost+httpNotifCost+emailNotifCost+sqsNotifCost;

    ////////////////////////////Additional Notif////////////////////////////////
    var mobilePushNotif=parseInt(req.body.mobilePushNotif);
    var mobilePushNotifCost=mobilePushNotif*0.0000005*1000000;
    var totalRounded=Math.round(mobilePushNotifCost);

    //////////////////Data transfer ////////////////////////////////////////
    var inBoundData=parseInt(req.body.inBoundData);
    var inBoundDataPrice=inBoundData*0;
    var outBoundDataType=parseInt(req.body.outBoundDataType);
    var outBoundDataAmount=parseInt(req.body.outBoundDataAmount);
    var outBoundDataCost;
    var outBoundDataCost1;
    var outBoundDataCost2;
    var outBoundDataCost3;
    var outBoundDataCost4;
    var totalData;
    if(outBoundDataType==1) {
        if (outBoundDataAmount == 0) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount == 1) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount <= 10239) {

            outBoundDataCost = (outBoundDataAmount - 1) * 0.09;
        }

        if (outBoundDataAmount > 10239 && outBoundDataAmount <= 51200) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = (outBoundDataAmount - 10240) * 0.085;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2;
        }

        if (outBoundDataAmount >= 51201) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = (outBoundDataAmount - (51199)) * 0.07;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3;
        }
        if (outBoundDataAmount >= 153600) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = 102400 * 0.07;
            outBoundDataCost4 = (outBoundDataAmount - (153599)) * 0.05;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3 + outBoundDataCost4;
        }
    }else if (outBoundDataType==2) {

        outBoundDataCost=0;

    }else if(outBoundDataType==3 || outBoundDataType==4 || outBoundDataType==5 || outBoundDataType==6 || outBoundDataType==7 || outBoundDataType==8 || outBoundDataType==9) {

        outBoundDataCost=outBoundDataAmount*0.01;
    }
    else if(outBoundDataType==10) {

        outBoundDataCost=outBoundDataAmount*0.02;
    }

    totalData=outBoundDataCost+inBoundDataPrice;


    var totalSNS=total+totalRounded+totalData;

    return res.status(200).json({
        totalSNS
    });

};

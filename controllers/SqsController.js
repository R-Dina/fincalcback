const express = require("express");
var session = require('express-session');



var app=express();


//The following method allows you to to calculate the service settings of  Amazon SQS consumption

exports.getSqsServiceSettings = (req, res, next) => {




    var stdardRequests=parseInt(req.body.stdardRequests);
    var fifoRequests=parseInt(req.body.fifoRequests);
    var stdardRequestsCost=stdardRequests*1000000*0.0000004000;
    var fifoRequestsCost=fifoRequests*1000000*0.0000005000;
    var total=stdardRequestsCost+fifoRequestsCost;
    return res.status(200).json({
       total
    });
};


//The following method allows you to to calculate the data transfer of  Amazon SQS consumption

exports.getSqsDataTransfer = (req, res, next) => {
    var inBoundData=parseInt(req.body.inBoundData);
    var inBoundDataPrice=inBoundData*0;
    var outBoundDataType=parseInt(req.body.outBoundDataType);
    var outBoundDataAmount=parseInt(req.body.outBoundDataAmount);
    var outBoundDataCost;
    var outBoundDataCost1;
    var outBoundDataCost2;
    var outBoundDataCost3;
    var outBoundDataCost4;
    var total;
    if(outBoundDataType==1) {
        if (outBoundDataAmount == 0) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount == 1) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount <= 10239) {

            outBoundDataCost = (outBoundDataAmount - 1) * 0.09;
        }

        if (outBoundDataAmount > 10239 && outBoundDataAmount <= 51200) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = (outBoundDataAmount - 10240) * 0.085;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2;
        }

        if (outBoundDataAmount >= 51201) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = (outBoundDataAmount - (51199)) * 0.07;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3;
        }
        if (outBoundDataAmount >= 153600) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = 102400 * 0.07;
            outBoundDataCost4 = (outBoundDataAmount - (153599)) * 0.05;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3 + outBoundDataCost4;
        }
    }else if (outBoundDataType==2) {

        outBoundDataCost=0;

    }else if(outBoundDataType==3 || outBoundDataType==4 || outBoundDataType==5 || outBoundDataType==6 || outBoundDataType==7 || outBoundDataType==8 || outBoundDataType==9) {

        outBoundDataCost=outBoundDataAmount*0.01;
    }
    else if(outBoundDataType==10) {

        outBoundDataCost=outBoundDataAmount*0.02;
    }
    total=outBoundDataCost+inBoundDataPrice;
    return res.status(200).json({
        total
    });
};


exports.getSqsTotal = (req, res, next) => {

    /////////////////////////////SQS service settings ///////////////////////
    var stdardRequests=parseInt(req.body.stdardRequests);
    var fifoRequests=parseInt(req.body.fifoRequests);
    var stdardRequestsCost=stdardRequests*1000000*0.0000004000;
    var fifoRequestsCost=fifoRequests*1000000*0.0000005000;
    var total=stdardRequestsCost+fifoRequestsCost;

    /////////////////////////////SQS data transfer //////////////////////////
    var inBoundData=parseInt(req.body.inBoundData);
    var inBoundDataPrice=inBoundData*0;
    var outBoundDataType=parseInt(req.body.outBoundDataType);
    var outBoundDataAmount=parseInt(req.body.outBoundDataAmount);
    var outBoundDataCost;
    var outBoundDataCost1;
    var outBoundDataCost2;
    var outBoundDataCost3;
    var outBoundDataCost4;
    var totalData;
    if(outBoundDataType==1) {
        if (outBoundDataAmount == 0) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount == 1) {

            outBoundDataCost == 0;
        }
        if (outBoundDataAmount <= 10239) {

            outBoundDataCost = (outBoundDataAmount - 1) * 0.09;
        }

        if (outBoundDataAmount > 10239 && outBoundDataAmount <= 51200) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = (outBoundDataAmount - 10240) * 0.085;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2;
        }

        if (outBoundDataAmount >= 51201) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = (outBoundDataAmount - (51199)) * 0.07;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3;
        }
        if (outBoundDataAmount >= 153600) {

            outBoundDataCost1 = 10239 * 0.09;
            outBoundDataCost2 = 40960 * 0.085;
            outBoundDataCost3 = 102400 * 0.07;
            outBoundDataCost4 = (outBoundDataAmount - (153599)) * 0.05;
            outBoundDataCost = outBoundDataCost1 + outBoundDataCost2 + outBoundDataCost3 + outBoundDataCost4;
        }
    }else if (outBoundDataType==2) {

        outBoundDataCost=0;

    }else if(outBoundDataType==3 || outBoundDataType==4 || outBoundDataType==5 || outBoundDataType==6 || outBoundDataType==7 || outBoundDataType==8 || outBoundDataType==9) {

        outBoundDataCost=outBoundDataAmount*0.01;
    }
    else if(outBoundDataType==10) {

        outBoundDataCost=outBoundDataAmount*0.02;
    }
    totalData=outBoundDataCost+inBoundDataPrice;

    var total=total+totalData;

    return res.status(200).json({
        total
    });

};

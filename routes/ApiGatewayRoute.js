const express = require("express");

const ApiGatewayController = require("../controllers/ApiGatewayController");


const router = express.Router();

router.post("/getApiGatewayHttp", ApiGatewayController.getApiGatewayHttp);

router.post("/getApiGatewayRest", ApiGatewayController.getApiGatewayRest);

router.post("/getApiGatewayWebSocket", ApiGatewayController.getApiGatewayWebSocket);

router.post("/getApiGatewayTotal", ApiGatewayController.getApiGatewayTotal);

module.exports = router;

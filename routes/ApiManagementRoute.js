const express = require("express");

const ApiManagementController = require("../controllers/ApiManagementController");


const router = express.Router();

router.post("/getApiManagement", ApiManagementController.getApiManagement);



module.exports = router;
const express = require("express");

const AuroraServerlessController = require("../controllers/AuroraServerlessController");


const router = express.Router();

////////////////Aurora PostgreSQL/////////////////

router.post("/getAuroraPosgreSQLSettings", AuroraServerlessController.getAuroraPosgreSQLSettings);
router.post("/getAuroraPosgreSQLdataStorage", AuroraServerlessController.getAuroraPosgreSQLdataStorage);
router.post("/getAuroraPosgreSQLbackupStorage", AuroraServerlessController.getAuroraPosgreSQLbackupStorage);
router.post("/getAuroraPosgreSQLtotal", AuroraServerlessController.getAuroraPosgreSQLtotal);

////////////////Aurora MySQL/////////////////
router.post("/getAuroraMySQLSettings", AuroraServerlessController.getAuroraMySQLSettings);
router.post("/getAuroraMySQLdataStorage", AuroraServerlessController.getAuroraMySQLdataStorage);
router.post("/getAuroraMySQLbackupStorage", AuroraServerlessController.getAuroraMySQLbackupStorage);
router.post("/getAuroraMySQLtotal", AuroraServerlessController.getAuroraMySQLtotal);



module.exports = router;

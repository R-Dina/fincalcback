const express = require("express");

const AzureFunctionsController = require("../controllers/AzureFunctionsController");


const router = express.Router();

router.post("/getAzureFunctions", AzureFunctionsController.getAzureFunctions);


module.exports = router;
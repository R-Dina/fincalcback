const express = require("express");

const AzureNotificationHubsController = require("../controllers/AzureNotificationHubsController");


const router = express.Router();

router.post("/getAzureNotificationHubsBasic", AzureNotificationHubsController.getAzureNotificationHubsBasic);
router.post("/getAzureNotificationHubsStandard", AzureNotificationHubsController.getAzureNotificationHubsStandard);




module.exports = router;
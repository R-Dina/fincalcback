const express = require("express");

const AzureServiceBusController = require("../controllers/AzureServiceBusController");


const router = express.Router();

router.post("/getAzureServiceBusBasic", AzureServiceBusController.getAzureServiceBusBasic);
router.post("/getAzureServiceBusStandard", AzureServiceBusController.getAzureServiceBusStandard);
router.post("/getAzureServiceBusPremium", AzureServiceBusController.getAzureServiceBusPremium);

module.exports = router;
const express = require("express");

const BlobStorageController = require("../controllers/BlobStorageController");


const router = express.Router();

router.post("/getBlobStorageStandard", BlobStorageController.getBlobStorageStandard);
router.post("/getBlobStorageStandardArchive", BlobStorageController.getBlobStorageStandardArchive);
router.post("/getBlobStorageStandardGRS", BlobStorageController.getBlobStorageStandardGRS);
router.post("/getBlobStorageStandardGRSArchive", BlobStorageController.getBlobStorageStandardGRSArchive);
router.post("/getBlobStorageStandardGzrsHot", BlobStorageController.getBlobStorageStandardGzrsHot);
router.post("/getBlobStorageStandardGzrsCool", BlobStorageController.getBlobStorageStandardGzrsCool);



module.exports = router;
const express = require("express");

const CosmoDbController = require("../controllers/CosmoDbController");


const router = express.Router();

router.post("/getCosmoDbServerless", CosmoDbController.getCosmoDbServerless);

module.exports = router;
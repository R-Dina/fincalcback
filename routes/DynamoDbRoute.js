const express = require("express");

const DynamoDbController = require("../controllers/DynamoDbController");


const router = express.Router();

router.post("/getDynamoDBOnDemandCapacityDataStorage", DynamoDbController.getDynamoDBOnDemandCapacityDataStorage);

router.post("/getDynamoDBOnDemandCapacityWriteSettings", DynamoDbController.getDynamoDBOnDemandCapacityWriteSettings);

router.post("/getDynamoDBOnDemandCapacityReadSettings", DynamoDbController.getDynamoDBOnDemandCapacityReadSettings);

router.post("/getDynamoDBOnDemandCapacityTotal", DynamoDbController.getDynamoDBOnDemandCapacityTotal);

router.post("/getDynamoDBProvisionedCapacityDataStorage", DynamoDbController.getDynamoDBProvisionedCapacityDataStorage);

router.post("/getDynamoDBProvisionedCapacityWriteSettings", DynamoDbController.getDynamoDBProvisionedCapacityWriteSettings);

router.post("/getDynamoDBProvisionedCapacityReadSettings", DynamoDbController.getDynamoDBProvisionedCapacityReadSettings);

router.post("/getDynamoDBProvisionedCapacityTotal", DynamoDbController.getDynamoDBProvisionedCapacityTotal);

router.post("/getDynamoDBTotal", DynamoDbController.getDynamoDBTotal);





module.exports = router;

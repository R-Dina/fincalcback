const express = require("express");

const LambdaController = require("../controllers/LambdaController");


const router = express.Router();

router.post("/getBasicLambda", LambdaController.getLambdaBasic);

router.post("/getLambdaProvConcurrency", LambdaController.getLambdaProvConcurrency);

router.post("/getLambdaTotal", LambdaController.getLambdaTotal);

module.exports = router;

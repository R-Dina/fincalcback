const express = require("express");

const QueueStorageController = require("../controllers/QueueStorageController");


const router = express.Router();

router.post("/getQueueStorageV1", QueueStorageController.getQueueStorageV1);
router.post("/getQueueStorageV2", QueueStorageController.getQueueStorageV2);

module.exports = router;
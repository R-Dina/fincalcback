const express = require("express");

const S3Controller = require("../controllers/S3Controller");


const router = express.Router();

router.post("/getS3Standard", S3Controller.getS3Standard);

router.post("/getS3Glacier", S3Controller.getS3Glacier);

router.post("/getS3Total", S3Controller.getS3Total);


module.exports = router;

const express = require("express");

const SnsController = require("../controllers/SnsController");


const router = express.Router();

router.post("/getSnsServiceSettings", SnsController.getSnsServiceSettings);

router.post("/getSnsAdditionalNotif", SnsController.getSnsAdditionalNotif);

router.post("/getSnsDataTransfer", SnsController.getSnsDataTransfer);

router.post("/getSnsTotal", SnsController.getSnsTotal);

module.exports = router;

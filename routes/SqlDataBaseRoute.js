const express = require("express");

const SqlDataBaseController = require("../controllers/SqlDataBaseController");


const router = express.Router();

router.post("/getSqlDataBase", SqlDataBaseController.getSqlDataBase);



module.exports = router;
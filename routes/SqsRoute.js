const express = require("express");

const SqsController = require("../controllers/SqsController");


const router = express.Router();

router.post("/getSqsServiceSettings", SqsController.getSqsServiceSettings);

router.post("/getSqsDataTransfer", SqsController.getSqsDataTransfer);

router.post("/getSqsTotal", SqsController.getSqsTotal);

module.exports = router;
